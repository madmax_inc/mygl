cmake_minimum_required(VERSION 2.8)
project(mygl-engine CXX)

set(CMAKE_CXX_STANDARD 11)

find_package(OpenGL REQUIRED)

include(${CMAKE_CURRENT_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

set(MATH_SOURCES
    include/math/matrix.hpp)

set(SOURCES
        include/exception.hpp
        include/types.hpp

        include/stopwatch.hpp
        src/stopwatch.cpp

        include/engine_loop.hpp
        src/engine_loop.cpp

        include/shader/shader_exception.hpp
        src/shader/shader_exception.cpp

        include/shader/shader_loader.hpp
        src/shader/shader_loader.cpp

        include/shader/shader_source_visitor.hpp

        include/shader/shader.hpp
        src/shader/shader.cpp

        include/shader/shader_params.hpp

        include/shader/shader_program.hpp
        src/shader/shader_program.cpp

        include/shader/i_shader_source.hpp

        include/shader/string_shader_source.hpp
        src/shader/string_shader_source.cpp

        include/geometry/baked_geometry.hpp
        src/geometry/baked_geometry.cpp
        include/geometry/geometry.hpp
        src/geometry/geometry.cpp
        include/geometry/triangle.hpp
        src/geometry/triangle.cpp

        include/engine_passkey.hpp
        include/engine.hpp
        src/engine.cpp)
set(LIBS ${OPENGL_gl_LIBRARY} mygl-common ${CONAN_LIBS})

include_directories(include src)
add_library(${PROJECT_NAME} SHARED ${SOURCES} ${MATH_SOURCES})
target_link_libraries(${PROJECT_NAME} ${LIBS})
target_include_directories(${PROJECT_NAME} PUBLIC include)
