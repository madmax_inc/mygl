#include "geometry/geometry.hpp"

namespace mygl {
namespace engine {
namespace geometry {

Geometry::Geometry(std::vector<Geometry::Vertex> vertices)
    : vertices_{std::move(vertices)} {}

const std::vector<Geometry::Vertex> Geometry::GetVertices() const {
  return vertices_;
}

} // namespace geometry
} // namespace engine
} // namespace mygl
