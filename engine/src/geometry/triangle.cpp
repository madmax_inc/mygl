#include "geometry/triangle.hpp"

#include <utility>

namespace mygl {
namespace engine {
namespace geometry {

Triangle::Triangle(TriangleVertices vertices)
    : Geometry{{begin(vertices), end(vertices)}} {}

} // namespace geometry
} // namespace engine
} // namespace mygl
