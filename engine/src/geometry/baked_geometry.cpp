#include "geometry/baked_geometry.hpp"

#include <GLES3/gl3.h>

namespace mygl {
namespace engine {
namespace geometry {
namespace {

BakedGeometry::VertexBuffer::Handle MakeBuffer(const Geometry &geometry) {
  type::UInt buffer_handle;
  glGenBuffers(1, &buffer_handle);
  glBindBuffer(GL_ARRAY_BUFFER, buffer_handle);
  const auto &vertices = geometry.GetVertices();
  glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Geometry::Vertex),
               reinterpret_cast<const void *>(vertices.data()), GL_STATIC_DRAW);
  return buffer_handle;
}

} // namespace

BakedGeometry::VertexBuffer::VertexBuffer(
    BakedGeometry::VertexBuffer::Handle handle)
    : handle_{handle} {}

BakedGeometry::VertexBuffer::~VertexBuffer() {
  glDeleteBuffers(1, handle_.operator->());
}

BakedGeometry::VertexBuffer::Handle
BakedGeometry::VertexBuffer::GetHandle() const {
  return handle_;
}

BakedGeometry::BakedGeometry(const Geometry &geometry, EnginePasskey)
    : vertex_buffer_{MakeBuffer(geometry)} {}

const BakedGeometry::VertexBuffer &BakedGeometry::GetBuffer() const {
  return vertex_buffer_;
}

} // namespace geometry
} // namespace engine
} // namespace mygl
