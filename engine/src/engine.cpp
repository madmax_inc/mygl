#include "engine.hpp"

#include <strong_typedef.hpp>
#include <utils/memory.hpp>

#include <GLES3/gl3.h>

#include "shader/shader_loader.hpp"
#include "types.hpp"

namespace mygl {
namespace engine {

namespace {

template <class ShaderType>
ShaderType MakeShader(ShaderType &&shader,
                      const shader::IShaderSource &source) {
  shader::ShaderLoader<ShaderType> loader{std::move(shader)};
  source.accept(loader);
  return std::move(loader).GetShader();
}

class VertexArrayObject {
public:
  struct Handle : common::StrongTypedef<type::UInt> {
    using common::StrongTypedef<type::UInt>::StrongTypedef;
  };

  explicit VertexArrayObject()
      : handle_{[] {
          type::UInt id;
          glGenVertexArrays(1, &id);
          glBindVertexArray(id);
          return id;
        }()} {}
  ~VertexArrayObject() { glDeleteVertexArrays(1, handle_.operator->()); }

private:
  Handle handle_;
};

} // namespace

struct Engine::Private {
  VertexArrayObject vao;
};

Engine::Engine(EnsureContextIsActive ensure_active)
    : ensure_context_is_active_{ensure_active} {
  ensure_context_is_active_();

  impl_ = common::utils::MakeUnique<Private>();
}

Engine::~Engine() = default;

shader::VertexShader
Engine::LoadVertexShader(const shader::IShaderSource &source) const {
  ensure_context_is_active_();
  return MakeShader(shader::VertexShader{{}}, source);
}

shader::FragmentShader
Engine::LoadFragmentShader(const shader::IShaderSource &source) const {
  ensure_context_is_active_();
  return MakeShader(shader::FragmentShader{{}}, source);
}

shader::ShaderProgram
Engine::LinkShaderProgram(shader::VertexShader vertex,
                          shader::FragmentShader fragment) const {
  ensure_context_is_active_();
  return shader::ShaderProgram{std::move(vertex), std::move(fragment), {}};
}

geometry::BakedGeometry
Engine::BakeGeometry(const geometry::Geometry &geometry) const {
  return geometry::BakedGeometry{geometry, {}};
}

void Engine::Render(const geometry::BakedGeometry &renderable,
                    const shader::ShaderProgram &shaders,
                    const shader::ShaderParams &shader_params) const {
  ensure_context_is_active_();

  glUseProgram(shaders.GetHandle());

  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, renderable.GetBuffer().GetHandle());
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

  glDrawArrays(GL_TRIANGLES, 0, 3);
  glDisableVertexAttribArray(0);
}

} // namespace engine
} // namespace mygl
