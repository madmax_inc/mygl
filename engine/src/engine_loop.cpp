#include "engine_loop.hpp"

#include <cstdint>

#include <GLES3/gl3.h>

#include "utils/memory.hpp"

namespace mygl {
namespace engine {

class EngineLoopBase::Private {
public:
  using OnNextCallback = std::function<void(std::chrono::milliseconds)>;
  using OnCompletedCallback = std::function<void(void)>;

  ::rxcpp::observable<std::chrono::milliseconds> Observe() {
    return ::rxcpp::rxs::create<std::chrono::milliseconds>(
        [this](::rxcpp::subscriber<std::chrono::milliseconds> s) {
          this->chain_.push_back(
              {[s](std::chrono::milliseconds delay) { s.on_next(delay); },

               [s] { s.on_completed(); }});
        });
  }

  void OnNextFrame(std::chrono::milliseconds e) const {
    for (const auto &events : chain_) {
      events.next(e);
    }
  }

  void OnCompleted() const {
    for (const auto &events : chain_) {
      events.completed();
    }
  }

private:
  struct Events {
    OnNextCallback next;
    OnCompletedCallback completed;
  };

  std::vector<Events> chain_;
};

EngineLoopBase::EngineLoopBase()
    : impl_{common::utils::MakeUnique<Private>()} {}

EngineLoopBase::~EngineLoopBase() = default;

::rxcpp::observable<std::chrono::milliseconds> EngineLoopBase::Observe() const {
  return impl_->Observe();
}

void EngineLoopBase::RenderFrame(std::chrono::milliseconds delay) const {
  glClear(GL_COLOR_BUFFER_BIT);
  impl_->OnNextFrame(delay);
}

void EngineLoopBase::Completed() const { impl_->OnCompleted(); }

} // namespace engine
} // namespace mygl
