#include "stopwatch.hpp"

namespace mygl {
namespace engine {

namespace {

struct TimeData {
  std::chrono::system_clock::time_point ts;
  std::chrono::milliseconds delay;
};

TimeData Measure(std::chrono::system_clock::time_point last_lap) {
  const auto now = std::chrono::system_clock::now();

  if (!last_lap.time_since_epoch().count()) {
    return {now, std::chrono::milliseconds::zero()};
  }

  const auto elapsed =
      std::chrono::duration_cast<std::chrono::milliseconds>(now - last_lap);

  return {now, elapsed};
}

} // namespace

std::chrono::milliseconds Stopwatch::CurrentTime() const {
  return Measure(last_lap_).delay;
}

std::chrono::milliseconds Stopwatch::Lap() {
  const auto time_data = Measure(last_lap_);

  last_lap_ = time_data.ts;

  return time_data.delay;
}

} // namespace engine
} // namespace mygl
