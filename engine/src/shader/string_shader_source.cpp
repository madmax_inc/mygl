#include "shader/string_shader_source.hpp"

#include "shader/shader_source_visitor.hpp"

namespace mygl {
namespace engine {
namespace shader {

StringShaderSource::StringShaderSource(std::string source)
    : source_{std::move(source)} {}

const std::string &StringShaderSource::GetSource() const { return source_; }

void StringShaderSource::accept(ShaderSourceVisitor &visitor) const {
  visitor.visit(*this);
}

} // namespace shader
} // namespace engine
} // namespace mygl
