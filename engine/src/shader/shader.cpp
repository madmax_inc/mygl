#include "shader/shader.hpp"

#include <GLES3/gl3.h>

namespace mygl {
namespace engine {
namespace shader {

namespace {

Shader::Handle MakeHandle(GLenum type) { return glCreateShader(type); }

} // namespace

Shader::Handle Shader::GetHandle() const { return *handle_; }

Shader::Shader(Shader::Handle handle) : handle_{handle} {}

Shader::~Shader() {
  if (handle_) {
    glDeleteShader(handle_);
  }
}

VertexShader::VertexShader(EnginePasskey)
    : Shader{MakeHandle(GL_VERTEX_SHADER)} {}

FragmentShader::FragmentShader(EnginePasskey)
    : Shader{MakeHandle(GL_FRAGMENT_SHADER)} {}

} // namespace shader
} // namespace engine
} // namespace mygl
