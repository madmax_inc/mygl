#include "shader/shader_exception.hpp"

namespace mygl {
namespace engine {
namespace shader {

ShaderCompilerIsNotSupported::ShaderCompilerIsNotSupported()
    : ShaderLoadException{"Shader compiler is not supported!"} {}

} // namespace shader
} // namespace engine
} // namespace mygl
