#include "shader/shader_program.hpp"

#include <GLES3/gl3.h>

namespace mygl {
namespace engine {
namespace shader {

ShaderProgram::ShaderProgram(VertexShader vertex, FragmentShader fragment,
                             EnginePasskey)
    : handle_{glCreateProgram()} {
  glAttachShader(handle_, vertex.GetHandle());
  glAttachShader(handle_, fragment.GetHandle());

  glLinkProgram(handle_);
}

ShaderProgram::~ShaderProgram() { glDeleteProgram(handle_); }

ShaderProgram::Handle ShaderProgram::GetHandle() const { return handle_; }

} // namespace shader
} // namespace engine
} // namespace mygl
