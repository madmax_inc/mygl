#include "shader/shader_loader.hpp"

#include <array>

#include <GLES3/gl3.h>

#include "shader/shader_exception.hpp"
#include "shader/string_shader_source.hpp"
#include "types.hpp"

namespace mygl {
namespace engine {
namespace shader {

namespace {

bool IsCompilerSupported() {
  type::Boolean supported;
  glGetBooleanv(GL_SHADER_COMPILER, &supported);
  return supported;
}

bool WasCompiled(Shader::Handle handle) {
  type::Int compile_status{};

  glGetShaderiv(handle, GL_COMPILE_STATUS, &compile_status);

  return compile_status != GL_FALSE;
}

std::string GetCompileErrorLog(Shader::Handle handle) {
  type::Int log_size{};

  glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &log_size);

  std::string result(log_size, '\0');
  glGetShaderInfoLog(handle, log_size, &log_size, result.begin().operator->());
  return result;
}

void ThrowIfNotCompiled(const Shader &shader) {
  if (WasCompiled(shader.GetHandle())) {
    return;
  }

  throw ShaderCompileError{GetCompileErrorLog(shader.GetHandle())};
}

} // namespace

template <class ShaderType>
ShaderLoader<ShaderType>::ShaderLoader(ShaderType &&shader)
    : shader_{std::move(shader)} {}

template <class ShaderType>
void ShaderLoader<ShaderType>::visit(const StringShaderSource &string_shader) {
  if (!IsCompilerSupported()) {
    throw ShaderCompilerIsNotSupported{};
  }

  std::array<const GLchar *, 1> sources = {string_shader.GetSource().c_str()};
  glShaderSource(shader_.GetHandle(), sources.size(), sources.data(), nullptr);
  glCompileShader(shader_.GetHandle());

  ThrowIfNotCompiled(shader_);
}

template <class ShaderType>
ShaderType ShaderLoader<ShaderType>::GetShader() && {
  return std::move(shader_);
}

template ShaderLoader<VertexShader>::ShaderLoader(VertexShader &&);
template void ShaderLoader<VertexShader>::visit(const StringShaderSource &);
template VertexShader ShaderLoader<VertexShader>::GetShader() &&;

template ShaderLoader<FragmentShader>::ShaderLoader(FragmentShader &&);
template void ShaderLoader<FragmentShader>::visit(const StringShaderSource &);
template FragmentShader ShaderLoader<FragmentShader>::GetShader() &&;

} // namespace shader
} // namespace engine
} // namespace mygl
