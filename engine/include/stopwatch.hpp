#pragma once

#include <chrono>

namespace mygl {
namespace engine {

class Stopwatch {
public:
  std::chrono::milliseconds CurrentTime() const;
  std::chrono::milliseconds Lap();

private:
  std::chrono::system_clock::time_point last_lap_{};
};

} // namespace engine
} // namespace mygl
