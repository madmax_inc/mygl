#pragma once

#include <strong_typedef.hpp>

#include "engine_passkey.hpp"
#include "shader/shader.hpp"

namespace mygl {
namespace engine {
namespace shader {

class ShaderProgram {
public:
  struct Handle : common::StrongTypedef<unsigned int> {
    using common::StrongTypedef<unsigned int>::StrongTypedef;
  };

  explicit ShaderProgram(VertexShader, FragmentShader, EnginePasskey);
  ~ShaderProgram();

  Handle GetHandle() const;

private:
  Handle handle_;
};

} // namespace shader
} // namespace engine
} // namespace mygl
