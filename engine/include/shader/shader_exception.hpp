#pragma once

#include "exception.hpp"

namespace mygl {
namespace engine {
namespace shader {

class ShaderLoadException : public MyglException {
  using MyglException::MyglException;
};

class ShaderCompilerIsNotSupported : public ShaderLoadException {
public:
  explicit ShaderCompilerIsNotSupported();
};

class ShaderCompileError : public ShaderLoadException {
  using ShaderLoadException::ShaderLoadException;
};

} // namespace shader
} // namespace engine
} // namespace mygl
