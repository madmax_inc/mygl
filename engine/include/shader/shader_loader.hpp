#pragma once

#include <is_one_of.hpp>

#include "shader.hpp"
#include "shader_source_visitor.hpp"

namespace mygl {
namespace engine {
namespace shader {

template <class ShaderType>
class ShaderLoader final : public ShaderSourceVisitor {
public:
  static_assert(common::IsOneOf<ShaderType, Shaders>::value, "");
  explicit ShaderLoader(ShaderType &&shader);

  void visit(const StringShaderSource &string_shader) final;

  ShaderType GetShader() &&;

private:
  ShaderType shader_;
};

} // namespace shader
} // namespace engine
} // namespace mygl
