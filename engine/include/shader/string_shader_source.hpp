#pragma once

#include <string>

#include "i_shader_source.hpp"

namespace mygl {
namespace engine {
namespace shader {

class StringShaderSource final : public IShaderSource {
public:
  explicit StringShaderSource(std::string source);

  const std::string &GetSource() const;

  void accept(ShaderSourceVisitor &) const final;

private:
  std::string source_;
};

} // namespace shader
} // namespace engine
} // namespace mygl
