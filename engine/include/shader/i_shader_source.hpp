#pragma once

namespace mygl {
namespace engine {
namespace shader {

class ShaderSourceVisitor;

class IShaderSource {
public:
  virtual ~IShaderSource() = default;

  virtual void accept(ShaderSourceVisitor &) const = 0;
};

} // namespace shader
} // namespace engine
} // namespace mygl
