#pragma once

namespace mygl {
namespace engine {
namespace shader {

class StringShaderSource;

class ShaderSourceVisitor {
public:
  virtual ~ShaderSourceVisitor() = default;

  virtual void visit(const StringShaderSource &) = 0;
};

} // namespace shader
} // namespace engine
} // namespace mygl
