#pragma once

#include <strong_typedef.hpp>
#include <utils/optional.hpp>
#include <variadic_typedef.hpp>

#include "engine_passkey.hpp"

namespace mygl {
namespace engine {
namespace shader {

class Shader {
public:
  struct Handle : common::StrongTypedef<unsigned int> {
    using common::StrongTypedef<unsigned int>::StrongTypedef;
  };

  Handle GetHandle() const;

protected:
  Shader(Handle);
  Shader(const Shader &) = delete;
  Shader &operator=(const Shader &) = delete;
  Shader(Shader &&) = default;
  ~Shader();

private:
  common::utils::Optional<Handle> handle_;
};

class VertexShader : public Shader {
public:
  explicit VertexShader(EnginePasskey);
};

class FragmentShader : public Shader {
public:
  explicit FragmentShader(EnginePasskey);
};

using Shaders = common::VariadicTypedef<VertexShader, FragmentShader>;

} // namespace shader
} // namespace engine
} // namespace mygl
