#pragma once

#include "geometry/geometry.hpp"
#include "math/matrix.hpp"
#include "types.hpp"

namespace mygl {
namespace engine {
namespace geometry {

class Triangle : public Geometry {
public:
  using TriangleVertices = std::array<Geometry::Vertex, 3>;

  explicit Triangle(TriangleVertices);
};

} // namespace geometry
} // namespace engine
} // namespace mygl
