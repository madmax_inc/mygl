#pragma once

#include <vector>

#include "math/matrix.hpp"
#include "types.hpp"

namespace mygl {
namespace engine {
namespace geometry {

class Geometry {
public:
  using Vertex = math::Vector<type::Float, 3>;

  explicit Geometry(std::vector<Vertex> vertices);
  const std::vector<Vertex> GetVertices() const;

private:
  std::vector<Vertex> vertices_;
};

} // namespace geometry
} // namespace engine
} // namespace mygl
