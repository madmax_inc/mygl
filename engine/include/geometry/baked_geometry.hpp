#pragma once

#include <strong_typedef.hpp>

#include "engine_passkey.hpp"
#include "geometry/geometry.hpp"
#include "types.hpp"

namespace mygl {
namespace engine {
namespace geometry {

class BakedGeometry {
public:
  class VertexBuffer {
  public:
    struct Handle : common::StrongTypedef<type::UInt> {
      using common::StrongTypedef<type::UInt>::StrongTypedef;
    };

    Handle GetHandle() const;

  private:
    Handle handle_;

    explicit VertexBuffer(Handle);
    ~VertexBuffer();

    friend class BakedGeometry;
  };

  explicit BakedGeometry(const Geometry &geometry, EnginePasskey);

  const VertexBuffer &GetBuffer() const;

private:
  VertexBuffer vertex_buffer_;
};

} // namespace geometry
} // namespace engine
} // namespace mygl
