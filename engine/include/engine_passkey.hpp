#pragma once

namespace mygl {
namespace engine {

class EnginePasskey {
private:
  EnginePasskey() = default;
  friend class Engine;
};

} // namespace engine
} // namespace mygl
