#pragma once

#include <GL/gl.h>

namespace mygl {
namespace engine {
namespace type {

using Boolean = GLboolean;
using Char = GLchar;
using Int = GLint;
using UInt = GLuint;
using Float = GLfloat;

} // namespace type
} // namespace engine
} // namespace mygl
