#pragma once

#include <stdexcept>

namespace mygl {
namespace engine {

class MyglException : public std::runtime_error {
  using std::runtime_error::runtime_error;
};

} // namespace engine
} // namespace mygl
