#pragma once

#include <chrono>
#include <memory>
#include <type_traits>

#include <rxcpp/rx.hpp>

#include "stopwatch.hpp"

namespace mygl {
namespace engine {

class EngineLoopBase {
public:
  ::rxcpp::observable<std::chrono::milliseconds> Observe() const;

protected:
  EngineLoopBase();
  ~EngineLoopBase();

  void RenderFrame(std::chrono::milliseconds delay) const;
  void Completed() const;

private:
  class Private;
  std::unique_ptr<Private> impl_;
};

template <class FpsSchedulingPolicy>
class EngineLoop : public EngineLoopBase, private FpsSchedulingPolicy {
public:
  EngineLoop(FpsSchedulingPolicy &&rhs = {})
      : FpsSchedulingPolicy{std::move(rhs)} {}

  template <class RenderingContext> void Run(RenderingContext &context) {
    context.MakeActive();

    Stopwatch stopwatch;

    do {
      FpsSchedulingPolicy::Schedule(const_cast<const Stopwatch &>(stopwatch));
      RenderFrame(stopwatch.Lap());

      context.SwapBuffer();
    } while (!context.ShouldClose());

    Completed();
  }
};

} // namespace engine
} // namespace mygl
