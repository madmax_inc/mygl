#pragma once

#include <functional>
#include <memory>

#include "geometry/baked_geometry.hpp"
#include "geometry/geometry.hpp"
#include "shader/i_shader_source.hpp"
#include "shader/shader.hpp"
#include "shader/shader_params.hpp"
#include "shader/shader_program.hpp"

namespace mygl {
namespace engine {

class Engine {
public:
  template <class RenderingContext>
  explicit Engine(RenderingContext &context)
      : Engine{[&context]() {
          if (!context.IsActive()) {
            context.MakeActive();
          }
        }} {}
  ~Engine();

  shader::VertexShader
  LoadVertexShader(const shader::IShaderSource &source) const;
  shader::FragmentShader
  LoadFragmentShader(const shader::IShaderSource &source) const;

  shader::ShaderProgram LinkShaderProgram(shader::VertexShader,
                                          shader::FragmentShader) const;

  geometry::BakedGeometry
  BakeGeometry(const geometry::Geometry &geometry) const;

  void Render(const geometry::BakedGeometry &renderable,
              const shader::ShaderProgram &shaders,
              const shader::ShaderParams &shader_params) const;

private:
  using EnsureContextIsActive = std::function<void(void)>;

  explicit Engine(EnsureContextIsActive);

  EnsureContextIsActive ensure_context_is_active_;

  struct Private;
  std::unique_ptr<Private> impl_;
};

} // namespace engine
} // namespace mygl
