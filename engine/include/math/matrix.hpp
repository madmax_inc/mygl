#pragma once

#include <array>
#include <cstdint>

namespace mygl {
namespace engine {
namespace math {

template <class T, std::size_t Rows, std::size_t Columns> class Matrix {
public:
  using RowType = std::array<T, Columns>;
  using DataType = std::array<RowType, Rows>;
  Matrix(DataType data) : data_{std::move(data)} {}

private:
  DataType data_;
};

template <class T, std::size_t N> class Vector : public Matrix<T, N, 1> {
public:
  Vector(std::array<T, N> data) : Matrix<T, N, 1>{MatrixData(data)} {}

private:
  static typename Matrix<T, N, 1>::DataType
  MatrixData(const std::array<T, N> &in) {
    typename Matrix<T, N, 1>::DataType data;

    for (auto i = 0; i < N; ++i) {
      data[i] = {in[i]};
    }

    return data;
  }
};

} // namespace math
} // namespace engine
} // namespace mygl
