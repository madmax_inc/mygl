cmake_minimum_required(VERSION 2.8)
project(mygl)

add_subdirectory(common)
add_subdirectory(engine)
add_subdirectory(backend/glfw)
add_subdirectory(example)
