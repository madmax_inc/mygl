#pragma once

#include <utility>

namespace mygl {
namespace common {

template <class T> class StrongTypedef {
public:
  StrongTypedef(T val) : impl_{std::move(val)} {}

  operator const T &() const { return *operator->(); }

  const T *operator->() const { return &impl_; }

private:
  T impl_;
};

} // namespace common
} // namespace mygl
