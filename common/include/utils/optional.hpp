#pragma once

#include <new>
#include <type_traits>
#include <utility>

namespace mygl {
namespace common {
namespace utils {

template <class T> class Optional;

template <class T> void swap(Optional<T> &lhs, Optional<T> &rhs) {
  std::swap(lhs.initialized_, rhs.initialized_);
  std::swap(lhs.impl_, rhs.impl_);
}

template <class T> class Optional {
public:
  Optional() : initialized_{false} {}

  Optional(const T &rhs) { Init(rhs); }

  Optional(T &&rhs) { Init(std::move(rhs)); }

  Optional(const Optional<T> &rhs) : initialized_{rhs.initialized_} {
    if (initialized_) {
      Init(*rhs);
    }
  }

  Optional(Optional<T> &&rhs) : initialized_{rhs.initialized_} {
    if (initialized_) {
      Init(std::move(*rhs));
      rhs.initialized_ = false;
    }
  }

  Optional<T> &operator=(Optional<T> rhs) {
    swap(*this, rhs);
    return *this;
  }

  ~Optional() {
    if (initialized_) {
      (**this).~T();
    }
  }

  operator bool() const { return initialized_; }

  template <class U> const T &ValueOr(U &&def) const {
    if (*this) {
      return **this;
    } else {
      return T{std::forward<U>(def)};
    }
  }

  T *operator->() { return reinterpret_cast<T *>(&impl_); }

  const T *operator->() const { return reinterpret_cast<const T *>(&impl_); }

  T &operator*() { return *operator->(); }

  const T &operator*() const { return *operator->(); }

private:
  template <class U> void Init(U &&rhs) {
    ::new (&impl_) T{std::forward<U>(rhs)};
    initialized_ = true;
  }

  typename std::aligned_storage<sizeof(T), alignof(T)>::type impl_;
  bool initialized_;

  friend void swap<>(Optional<T> &, Optional<T> &);
};

} // namespace utils
} // namespace common
} // namespace mygl
