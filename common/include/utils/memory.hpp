#pragma once

#include <memory>

namespace mygl {
namespace common {
namespace utils {

template <class T, class... Args>
std::unique_ptr<T> MakeUnique(Args &&... args) {
  return std::unique_ptr<T>{new T{std::forward<Args>(args)...}};
}

} // namespace utils
} // namespace common
} // namespace mygl
