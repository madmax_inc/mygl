#pragma once

#include <cstdint>

namespace mygl {
namespace common {

struct Version {
  std::size_t major;
  std::size_t minor;
};

} // namespace common
} // namespace mygl
