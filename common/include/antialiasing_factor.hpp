#pragma once

#include "strong_typedef.hpp"

namespace mygl {
namespace common {

struct AntialisingFactor : StrongTypedef<std::size_t> {
  using StrongTypedef<std::size_t>::StrongTypedef;
};

} // namespace common
} // namespace mygl
