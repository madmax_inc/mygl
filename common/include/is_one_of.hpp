#pragma once

#include <type_traits>

#include "variadic_typedef.hpp"

namespace mygl {
namespace common {

template <class T, class... Args> struct IsOneOf {};

template <class T, class... Args>
struct IsOneOf<T, VariadicTypedef<Args...>> : IsOneOf<T, Args...> {};

template <class T, class Arg> struct IsOneOf<T, Arg> : std::is_same<T, Arg> {};

template <class T, class First, class... Args>
struct IsOneOf<T, First, Args...>
    : std::conditional<IsOneOf<T, First>::value, std::true_type,
                       IsOneOf<T, Args...>>::type {};

} // namespace common
} // namespace mygl
