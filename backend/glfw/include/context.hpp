#pragma once

#include <memory>

#include "rendering_context.hpp"

namespace mygl {
namespace backend {
namespace glfw {

/**
 * @brief The Context class
 * The context should be initialized first and inherit the whole app's lifetime
 * It is the only way to instantinate rendering contexts (eg windows)
 */
class Context {
public:
  Context();
  ~Context();

  RenderingContext &MakeRenderingContext(const RenderingContext::Config &);

private:
  struct Private;
  std::unique_ptr<Private> d_;
};

} // namespace glfw
} // namespace backend
} // namespace mygl
