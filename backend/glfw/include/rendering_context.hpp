#pragma once

#include <memory>
#include <string>

#include "antialiasing_factor.hpp"
#include "version.hpp"

namespace mygl {
namespace backend {
namespace glfw {

class RenderingContext {
public:
  class Passkey {
  private:
    Passkey() = default;
    friend class Context;
  };

  struct Config {
    common::Version gl_version;
    common::AntialisingFactor antialiasing_factor;

    struct {
      std::size_t width;
      std::size_t height;
    } size;

    std::string title;
  };

  explicit RenderingContext(const Config &, Passkey);
  RenderingContext(RenderingContext &&rhs);
  ~RenderingContext();

  bool IsActive() const;
  void MakeActive();
  void SwapBuffer();
  bool ShouldClose() const;

private:
  struct Private;
  std::unique_ptr<Private> impl_;
};

} // namespace glfw
} // namespace backend
} // namespace mygl
