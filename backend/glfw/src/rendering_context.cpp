#include "rendering_context.hpp"

#include <stdexcept>
#include <thread>

#include <GLFW/glfw3.h>

#include "utils/memory.hpp"

namespace mygl {
namespace backend {
namespace glfw {

namespace {

GLFWwindow *MakeWindow(const RenderingContext::Config &config) {
  glfwWindowHint(GLFW_SAMPLES, config.antialiasing_factor);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, config.gl_version.major);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, config.gl_version.minor);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT,
                 GL_TRUE); // To make MacOS happy; should not be needed
  glfwWindowHint(GLFW_OPENGL_PROFILE,
                 GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL

  // Open a window and create its OpenGL context
  auto *window = glfwCreateWindow(config.size.width, config.size.height,
                                  config.title.c_str(), nullptr, nullptr);
  if (!window) {
    throw std::runtime_error{""};
  }

  return window;
}

} // namespace

struct RenderingContext::Private {
  GLFWwindow *window;
  std::thread::id thread_id{};
  bool was_initialized{};

  Private(GLFWwindow *window) : window{window} {}

  void Initialize() {
    was_initialized = true;
    thread_id = std::this_thread::get_id();
  }
};

RenderingContext::RenderingContext(const Config &config, Passkey)
    : impl_{common::utils::MakeUnique<Private>(Private{MakeWindow(config)})} {}

RenderingContext::RenderingContext(RenderingContext &&rhs)
    : impl_{std::move(rhs.impl_)} {}

bool RenderingContext::IsActive() const {
  if (!impl_->was_initialized) {
    return false;
  }

  const auto current_thread_id = std::this_thread::get_id();
  return current_thread_id == impl_->thread_id;
}

void RenderingContext::MakeActive() {
  auto *window = impl_->window;
  glfwMakeContextCurrent(window);

  impl_->Initialize();

  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
}

void RenderingContext::SwapBuffer() {
  glfwSwapBuffers(impl_->window);
  glfwPollEvents();
}

bool RenderingContext::ShouldClose() const {
  auto *window = impl_->window;
  return glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS ||
         glfwWindowShouldClose(window);
}

RenderingContext::~RenderingContext() = default;

} // namespace glfw
} // namespace backend
} // namespace mygl
