#include "context.hpp"

#include <stdexcept>
#include <vector>

#include <GLFW/glfw3.h>

#include "utils/memory.hpp"

namespace mygl {
namespace backend {
namespace glfw {

struct Context::Private {
  std::vector<RenderingContext> windows_;
};

Context::Context() : d_{common::utils::MakeUnique<Private>()} {
  if (!glfwInit()) {
    throw std::runtime_error{""};
  }
}

Context::~Context() { glfwTerminate(); }

RenderingContext &
Context::MakeRenderingContext(const RenderingContext::Config &config) {
  d_->windows_.push_back(RenderingContext{config, {}});
  return *(--d_->windows_.end());
}

} // namespace glfw
} // namespace backend
} // namespace mygl
