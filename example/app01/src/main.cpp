#include <iostream>

#include <context.hpp>
#include <engine.hpp>
#include <engine_loop.hpp>
#include <geometry/triangle.hpp>
#include <shader/string_shader_source.hpp>

class UnlimitedFpsSchedulingPolicy {
public:
  void Schedule(const mygl::engine::Stopwatch &) const {}
};

constexpr auto kVertexShader = R"(
#version 330 core

in vec3 vertexPosition_modelspace;

void main() {
    gl_Position.xyz = vertexPosition_modelspace;
    gl_Position.w = 1.0;
}
)";
constexpr auto kFragmentShader = R"(
                                 #version 330 core
                                 out vec3 color;
                                 void main(){
                                   color = vec3(1,0,0);
                                 }
                                 )";

int main() {
  mygl::backend::glfw::Context ctx;
  auto &rendering_context =
      ctx.MakeRenderingContext({/*.version = */ {3, 3},
                                /*.aliasing_factor = */ 4,
                                /*.size = */ {1024, 768},
                                /*.title = */ "Example App 01"});

  mygl::engine::Engine engine{rendering_context};

  const auto shader_program = [&engine]() {
    auto vertex = engine.LoadVertexShader(
        mygl::engine::shader::StringShaderSource{kVertexShader});
    auto fragment = engine.LoadFragmentShader(
        mygl::engine::shader::StringShaderSource{kFragmentShader});

    return engine.LinkShaderProgram(std::move(vertex), std::move(fragment));
  }();

  mygl::engine::EngineLoop<UnlimitedFpsSchedulingPolicy> pipeline;

  pipeline.Observe().subscribe(
      [&engine, &shader_program](std::chrono::milliseconds /*delay*/) {
        static const auto triangle =
            engine.BakeGeometry(mygl::engine::geometry::Triangle{{{
                {{-1, -1, 0}},
                {{0, 1, 0}},
                {{1, -1, 0}},
            }}});
        engine.Render(triangle, shader_program, {});
      });

  pipeline.Run(rendering_context);
}
